#!/usr/bin/env python3

from Crypto.Hash import SHA, SHA256, SHA384, SHA512
from datetime import datetime
from pyasn1.codec.der import decoder as der_decoder
from pyasn1.codec.der import encoder as der_encoder
from pyasn1.type.univ import ObjectIdentifier
from pyasn1_modules import pem
from pyasn1_modules import rfc2459
from pyasn1_modules.rfc2459 import id_ce_basicConstraints as OID_BASIC_CONSTRAINTS
from pyasn1_modules.rfc2459 import id_at_commonName as OID_COMMON_NAME
from pyasn1_modules.rfc2459 import rsaEncryption as OID_RSA_ENCRYPTION
from pyasn1_modules.rfc3447 import RSAPublicKey
from re import sub
from sys import argv

# TODO -- Z!!!!!!
_utc_p_format = '%y%m%d%H%M%SZ'
_utc_f_format = '%d/%m/%Y'

_rsa_signing_algorithms = {
	rfc2459.sha1WithRSAEncryption: SHA,  # defined in RFC 2437 (obsoleted by RFC 3447)
	ObjectIdentifier('1.2.840.113549.1.1.11'): SHA256,  # defined in RFC 3447
	ObjectIdentifier('1.2.840.113549.1.1.12'): SHA384,  # defined in RFC 3447
	ObjectIdentifier('1.2.840.113549.1.1.13'): SHA512}  # defined in RFC 3447


def get_issuer(certificate):
	tbs_certificate = certificate['tbsCertificate']
	issuer = tbs_certificate['issuer']

	return _common_name(issuer)


def get_subject(certificate):
	tbs_certificate = certificate['tbsCertificate']
	subject = tbs_certificate['subject']

	return _common_name(subject)


def _common_name(field):
	field = field.getComponent()

	value = _find__common_name(field)

	ds, rest = der_decoder.decode(value, asn1Spec=rfc2459.DirectoryString())
	assert len(rest) == 0

	return bytes(ds.getComponent()).decode()


def _find__common_name(field):
	for relative_distinguished_name in field:
		for attribute_type_and_value in relative_distinguished_name:
			oid = attribute_type_and_value['type']
			if oid == OID_COMMON_NAME:
				return attribute_type_and_value['value']


def get_validity(certificate):
	tbs_certificate = certificate['tbsCertificate']
	val = tbs_certificate['validity']

	yield _validity_get_field(val, 'notBefore')
	yield _validity_get_field(val, 'notAfter')


def _validity_get_field(validity_cert, field_name):
	field = validity_cert[field_name]
	field = field.getComponent()

	utc_time_str = field.asOctets().decode()

	utc_time = datetime.strptime(utc_time_str, _utc_p_format)

	return utc_time


def _format_date(date):
	return datetime.strftime(date, _utc_f_format)


def read_cert_from_file(filename):
	with open(filename) as f:
		binary_data = pem.readPemFromFile(f)
		cert, rest = der_decoder.decode(binary_data, asn1Spec=rfc2459.Certificate())
		assert len(rest) == 0

		return cert


def get_is_ca(certificate):
	tbs_certificate = certificate['tbsCertificate']
	extensions = tbs_certificate['extensions']
	ext_val = _find_ca(extensions)

	octet_string, rest = der_decoder.decode(ext_val)
	assert len(rest) == 0

	basic_constraints, rest = der_decoder.decode(octet_string, asn1Spec=rfc2459.BasicConstraints())
	assert len(rest) == 0

	return bool(basic_constraints['cA'])


def _find_ca(extensions):
	for extension in extensions:
		if extension['extnID'] == OID_BASIC_CONSTRAINTS:
			return extension['extnValue']


def print_data(certificate):
	print('Issuer:', get_issuer(certificate))

	print('Subject:', get_subject(certificate))

	print('Validity: from {0} to {1}'.format(*(_format_date(date) for date in get_validity(certificate))))

	if get_is_ca(certificate):
		print('Can be used for signing other certificates')


def _read_certificates(certificate_names):
	certificates = []

	for certificate_name in certificate_names:
		certificates.append(read_cert_from_file(certificate_name))

	return certificates


def _from_bitstring_to_bytes(bs):
	i = int("".join(str(bit) for bit in bs), base=2)
	return i.to_bytes((i.bit_length() + 7) // 8, byteorder='big')


def get_rsa_public_key(certificate):
	tbs_certificate = certificate['tbsCertificate']
	subject_pk = tbs_certificate['subjectPublicKeyInfo']
	algorithm_oid = subject_pk['algorithm']['algorithm']

	assert algorithm_oid == OID_RSA_ENCRYPTION

	pk = _from_bitstring_to_bytes(subject_pk['subjectPublicKey'])

	rsa_pk, rsa_rest = der_decoder.decode(pk, asn1Spec=RSAPublicKey())
	assert len(rsa_rest) == 0

	cert_modulus = hex(rsa_pk['modulus'])
	cert_exponent = rsa_pk['publicExponent']

	return cert_modulus, cert_exponent


def hash_from_certificate(certificate):
	tbs_cert = certificate['tbsCertificate']
	signature_algorithm = certificate['signatureAlgorithm']
	algorithm_oid = signature_algorithm['algorithm']

	rsa_signing_algorithm = _rsa_signing_algorithms[algorithm_oid].new()
	rsa_signing_algorithm.update(der_encoder.encode(tbs_cert))

	return rsa_signing_algorithm.hexdigest()


def hash_from_signature(certificate, key_store):
	signature_value = certificate['signatureValue']
	signature_value = int("".join(str(bit) for bit in signature_value), base=2)  # to int

	issuer_modulus, issuer_exponent = key_store[get_issuer(certificate)]

	issuer_exponent = int(issuer_exponent)
	issuer_modulus = int(issuer_modulus, 16)

	signature_value = pow(signature_value, issuer_exponent, issuer_modulus)
	return hex(signature_value)


def verify_certificate(certificate, key_store):
	print('Verifying {}'.format(get_subject(certificate)))

	validity_from, validity_to = get_validity(certificate)
	if not validity_from <= datetime.today() <= validity_to:
		print('  Certificate expired!')
		# no need to check if certificate is authentic, since we won't be using it anyway
		return

	h_tbs = hash_from_certificate(certificate)

	signature_hash = hash_from_signature(certificate, key_store)
	h_sv = sub('0x1f*00', '', signature_hash)
	h_sv = h_sv[-len(h_tbs):]

	if h_sv != h_tbs:
		print('    Certificate signature not matching!')
	else:
		if get_issuer(certificate) == get_subject(certificate):
			print('    Certificate self-signed')
		else:
			print('    Certificate signed by {}'.format(get_issuer(certificate)))


def main():
	certificates = []
	key_store = {}

	# print certificates summary
	for filename in argv[1:]:
		certificate = read_cert_from_file(filename)
		certificates.append(certificate)

		print('=== {0} ==='.format(filename))
		print_data(certificate)
		print()  # empty line

		# check which certificates can be used for signing
		if get_is_ca(certificate):
			key_store[get_subject(certificate)] = get_rsa_public_key(certificate)

	for certificate in certificates:
		verify_certificate(certificate, key_store)


if __name__ == '__main__':
	main()
