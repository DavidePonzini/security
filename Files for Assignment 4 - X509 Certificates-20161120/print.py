import os
import inspect
from pyasn1_modules import pem
from pyasn1_modules import rfc2459
from pyasn1.type import univ, tag
from x509_certificates import common_name
from pyasn1.codec.der import decoder as der_decoder


CERT_DIR = 'example_certificates'
filename = os.path.join(CERT_DIR, 'amazon.pem')

with open(filename) as f:
    binary_data = pem.readPemFromFile(f)
# print(binary_data)


#       ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


#   print(inspect.getsource(rfc2459.Certificate))

cert, rest = der_decoder.decode(binary_data, asn1Spec=rfc2459.Certificate())
assert len(rest) == 0

tbs_cert = cert['tbsCertificate']

#   print(inspect.getsource(rfc2459.TBSCertificate))
#   print('Version:', tbs_cert['version'].prettyPrint())
#   print('issuer='+tbs_cert['issuer'].prettyPrint())

issuer = tbs_cert['issuer']
#   print(issuer.prettyPrint())

subject = tbs_cert['subject']

print('Issuer:', common_name(issuer))
print('Subject:', common_name(subject))

