from pyasn1_modules import rfc2459
from pyasn1.codec.der import decoder as der_decoder
from pyasn1_modules.rfc2459 import id_at_commonName as OID_COMMON_NAME


def common_name(issuer):
    issuer = issuer.getComponent()

    for relative_distinguished_name in issuer:
        for attribute_type_and_value in relative_distinguished_name:
            oid = attribute_type_and_value['type']
            if oid == OID_COMMON_NAME:
                value = attribute_type_and_value['value']

    ds, rest = der_decoder.decode(value, asn1Spec=rfc2459.DirectoryString())
    assert len(rest) == 0
    ds = bytes(ds.getComponent()).decode()

    return ds
