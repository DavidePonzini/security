#!/usr/bin/env python3

import binascii
import collections



def unhex(a):
    return binascii.unhexlify(a.encode('ascii'))

def xor(a,b):
    return bytes(x^y for (x,y) in zip(a,b))

def strxor(a,b):
    h1 = unhex(a)
    h2 = unhex(b)
    return xor(h1,h2)


def findKey(ciphers):
	# maximum cipher length
	cipher_len = max(len(binascii.unhexlify(cipher.encode('ascii'))) for cipher in ciphers)

	# stores the final key
	final_key = [None] * cipher_len

	# how many ... < whatever -.- >
	precision = len(ciphers) / 2

	# a byte string of spaces, long cipher_len (so we don't need to create it several times)
	space_str = (binascii.hexlify(' '.encode('ascii'))*cipher_len).decode('ascii')

	for idx, cipher in enumerate(ciphers):

		counter = collections.Counter()

		for idx2, cipher2 in enumerate(ciphers):
			# don't xor ciphertext with itself
			if idx == idx2:
				continue

			cipher_xor = strxor(cipher, cipher2)
#			print('xor({}, {}): {}'.format(idx, idx2, cipher_xor))

			for idx_char, char in enumerate(cipher_xor):
#				print('  {} = {}'.format(idx_char, chr(char)))
				
				if chr(char).isalpha():
					counter[idx_char] += 1

#			print(counter.items())

		space_idx = []

		for counter_idx, counter_val in counter.items():
			if counter_val > precision:
				space_idx.append(counter_idx)	# there's a space here
		print('Spaces are at {}'.format(space_idx))

		cipher_xor_key = strxor(cipher, space_str)
#		print(cipher_xor_key)

		for idx in space_idx:
			if final_key[idx] == None:
				final_key[idx] = cipher_xor_key[idx:idx+1]

	final_key_bytes = bytes()

	for byte in final_key:
		if byte == None:
			final_key_bytes += b'\x00'
		else:
			final_key_bytes += byte

	return binascii.hexlify(final_key_bytes)


def decrypt(text, key):
	return strxor(text, key.decode('ascii')).decode('ascii')


def get_decryption_idx(max_idx):
	while True:
		try:
			choice = int(input())
		except ValueError:
			print('Invalid value')
		else:
			if 0 <= choice < max_idx:
				return choice
			else:
				print('Index must be between {} and {}'.format(0, max_idx))


def main():
	with open('ciphertexts.txt') as file:
		ciphers = file.read().splitlines()

	key = findKey(ciphers)
	print('Key: {}'.format(key.decode('ascii')))


	### semplicemente ignoralo ###
	"""
	cipher_len = max(len(cipher) for cipher in ciphers)

	ciphers_maxlen = []
	idx = 0

	for cipher in ciphers:
		if len(cipher) == cipher_len:
			print('{} - "{}"'.format(idx, strxor(cipher, key.decode())))

			ciphers_maxlen.append(cipher)
			idx += 1
		
	print('bla bla? {}'.format(len(ciphers_maxlen)))
	choice = get_decryption_idx(len(ciphers_maxlen))

	cleartext = input()

	print('cleartext: "{}"'.format(cleartext))

	key = binascii.hexlify(strxor(binascii.hexlify(cleartext.encode('ascii')).decode('ascii'), ciphers_maxlen[choice]))
	"""
	for cipher in ciphers:
		print(decrypt(cipher, key))

	print('key is ', key.decode('ascii'), sep='')


if __name__ == '__main__':
	main()
