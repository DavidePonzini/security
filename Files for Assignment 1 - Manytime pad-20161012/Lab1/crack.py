#!/usr/bin/env python2.7

import string
import collections
import sets


# XORs two strings, trimming the longer input
def strxor(a, b):
    return "".join([chr(ord(x) ^ ord(y)) for (x, y) in zip(a, b)])


# Read from input file
with open('ciphertexts.txt') as file:
	lines = file.read().splitlines()

# unknown ciphertexts (in hex format), all encrpyted with the same key
ciphers = []

# skip empty lines from input file
for line in lines:
	if line != '':
		ciphers.append(line)

# The target ciphertext we want to crack
target_cipher = ciphers[8]

# To store the final key
final_key = [None]*150

# To store the positions we know are broken
known_key_positions = set()


# For each ciphertext
for current_index, ciphertext in enumerate(ciphers):

	counter = collections.Counter()
	# for each other ciphertext
	for index, ciphertext2 in enumerate(ciphers):
		if current_index != index: # don't xor a ciphertext with itself
			for indexOfChar, char in enumerate(strxor(ciphertext.decode('hex'), ciphertext2.decode('hex'))): # Xor the two ciphertexts
				# If a character in the xored result is a alphanumeric character, it means there was probably a space character in one of the plaintexts (we don't know which one)
				if char in string.printable and char.isalpha():
					counter[indexOfChar] += 1 # Increment the counter at this index

	knownSpaceIndexes = []
	# Loop through all positions where a space character was possible in the current_index cipher
	for ind, val in counter.items():
		# If a space was found at least 6 times at this index, then the space character was likely from the current_index cipher!
			# We chose 6 because we noticed it gives us a clearer text
		if val >= 6: knownSpaceIndexes.append(ind)
#	print knownSpaceIndexes # Shows all the positions where we now know the key!

	# Now Xor the current_index with spaces, and at the knownSpaceIndexes positions we get the key back!
	xor_with_spaces = strxor(ciphertext.decode('hex'),' '*150)
	for index in knownSpaceIndexes:
		# Store the key's value at the correct position
		final_key[index] = xor_with_spaces[index].encode('hex')
		# Record that we known the key at this position
		known_key_positions.add(index)

# Construct a hex key from the currently known key, adding in '00' hex chars where we do not know (to make a complete hex string)
final_key_hex = ''.join([val if val is not None else '00' for val in final_key])

# Xor the currently known key with the target cipher
output = strxor(target_cipher.decode('hex'),final_key_hex.decode('hex'))

# Print the output, printing a * if that character is not known yet
print "\n# Here is the output before guessing: \n"
print ''.join([char if index in known_key_positions else '*' for index, char in enumerate(output)])


# Manual step

# From the output this prints, we can manually complete the target plaintext from (e.g.):
# toLinnovation    like in a proprietary setting  and I thin* ****
# to:
# to innovation    like in a proprietary setting  and I think Open

# We then confirm this is correct by producing the key from this, and decrpyting all the other messages to ensure they make grammatical sense
target_plaintext = "to innovation    like in a proprietary setting  and I think Open"

print "\n# Here is the output after guessing: \n"
print target_plaintext

print "\n\n# Decryption: \n"

key = strxor(target_cipher.decode('hex'),target_plaintext)

for cipher in ciphers:
        print strxor(cipher.decode('hex'),key)

print "\n# We thank the journalist for the interview :-)  \nwww.informationweek.com/the-torvalds-transcript-why-i-absolutely-love-gpl-version-2/d/d-id/1053128?"
