#!/usr/bin/env python3


# Esercizio svolto da:
#   Federico Casano
#   Davide Ponzini


import sys
import gzip
import string
import itertools
import time
from collections import namedtuple
from Crypto.Hash import MD2

UserData = namedtuple('UserData', 'username hashed_password')

with gzip.open('small_ita_lower.gz', mode='rt') as f:
    ita_words = [line.strip() for line in f]

#with open('top_passwords.txt', mode='rt') as txt:      # initialize top_passwords, reading from file
#    top_passwords = [line.st1rip() for line in txt]


def terribly_bad_hash(password):
    md2 = MD2.new()
    md2.update(password.encode())
    return md2.hexdigest()[: -3]


def read_shadow_file(filename):
    print('Reading', filename)
    data = []
    with open(filename) as shadow_file:
        for line in shadow_file:
            fields = line.rstrip().split(':')
            username = fields[0]
            hashed_password = fields[1]
            if not hashed_password:
                print('Empty password for user {}!'.format(username))
                continue
            if hashed_password in ['*', '!']:
                continue
            data.append(UserData(username, hashed_password))
    return data


def main(shadow_filename, hash_func=terribly_bad_hash):
    start = time.time()
    user_data = read_shadow_file(shadow_filename)
    n_users = len(user_data)
    cracked_usernames = set()
    n_tries = 0

    def found_password():
        cracked_usernames.add(user.username)
        delta_t = time.time() - start
        n_found_pw = len(cracked_usernames)
        print('Found password for user {}: {}'.format(user.username, guess))
        done = n_found_pw == n_users
        if delta_t > 5 or done:
            print('Found {} password, out of {}, in {:.2f} seconds ({} guesses/s)'.format(n_found_pw, n_users, delta_t,
                                                                                          int(n_tries / delta_t)))
        if done:
            print('DONE! :-)')
            sys.exit(0)

    for user in user_data:
        for guess in guesses_from_username(user.username):
            n_tries += 1
            guess_hash = hash_func(guess)
            if guess_hash == user.hashed_password:
                found_password()
                break
    guess_generators = [
        really_stupid_ones,
        most_common,
        most_common_plus_year,
        italian_dict,
        dicts_with_replace,
        wild_guesses,
        brute_force
    ]
    for gen in guess_generators:
        print('Trying', gen.__name__)
        for guess in gen():
            print('Guess:', guess)
            guess_hash = hash_func(guess)
            n_tries += 1
            for user in user_data:
                if guess_hash == user.hashed_password:
                    found_password()
    print("Couldn't find the password of:", {u.username for u in user_data} - cracked_usernames)


def guesses_from_username(username):
    up_low = map(''.join, itertools.product(*((c.upper(), c.lower()) for c in username)))
    yield from up_low

    # usernames with number
    for i in range(10000):
        yield username + str(i)
        yield str(i) + username
    
    # change some characters
    intab =  ['aeio', 'aeio', 'aeio', 'aeio', '']        # letters to be replaced
    outtab = ['@310', '4310', '@3!0', '23!0', '']        # letters to replace with
    
    for i,o in zip(intab, outtab):
        trantab = str.maketrans(i, o)
        yield username.translate(trantab)
    
    
    # Add something at the end
    numbers = string.digits
    letters = string.ascii_letters
    symbols = '!@#$%^&*()-_=+[]{};:\'"\\|,.<>/?`~'

    characters = list(numbers + letters + symbols)
    
    for i in range(1, 3):
        for c in itertools.permutations(characters, i):
            yield username + ''.join(c)


def really_stupid_ones():  # add more stupid passwords
    with open('stupid_passwords.txt', mode='rt') as filename:
        stupid_words = [line.strip() for line in filename]
    return stupid_words


def italian_dict():     # return the italian words (read from the file)
    return ita_words


def most_common():  # return the most common passwords (read from the file)
    with gzip.open('100k_most_common.txt.gz', mode='rt') as file:
        common_words = [line.strip() for line in file]
    return common_words


def dicts_with_replace():       # return dictionary words, where some letters have been replaced with numbers (e.g. a -> 4, o -> 0, ...)
    intab = "aeio"
    outtab = "4310"
    trantab = str.maketrans(intab, outtab)

    for i in ita_words:
        yield i.translate(trantab)


def most_common_plus_year():  # append a year to common passwords
    for item in most_common():
        for year in range(1900, 2018):
            yield item + str(year)


def brute_force():  # return all possible strings (of reasonable symbols) UP TO FOUR CHARS;
    numbers = string.digits
    letters = string.ascii_letters
    symbols = '!@#$%^&*()-_=+[]{};:\'"\\|,.<>/?`~'

    characters = list(numbers + letters + symbols)

    for i in range(1, 5):
        for c in itertools.permutations(characters, i):
            yield (''.join(c))


def wild_guesses():     # reason about the user-names and add wild guesses ;-)
    guesses = [
        'Giovanni', 'giovanni', 'paperino', 'ios', 'ilgrigio', 'ilbianco', 'Mascetti', 'mascetti',
        'supercazzola', 'perozzi', 'paperino', 'sec', 'security', 'torvalds', 'Torvalds', 'vanPelt', 'vanpelt', 'linux',
        'kernel',
        'vf', 'frodo', 'lagorio', 'chiola', 'gio', 'cl', 'lc', 'quite-secret', 'secret', '2000', 'amicimiei', 'foo',
        'their_sql',
        'our_sql', 'his_sql', 'her_sql', 'its_sql', 'Mouse', 'Minnie', 'amici miei', 'raffaello.mascetti',
        'alice.mascetti', 'ugo.tognazzi', 'molli.pucci']
    yield from guesses


if __name__ == '__main__':
    main(sys.argv[1] if len(sys.argv) == 2 else 'cns_shadow_file')
