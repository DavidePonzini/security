#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>

typedef struct {
	int is_root;
	/* ... */
} userinfo;

void init_ui(userinfo *ui) {
	ui->is_root = getuid()==0;
	/* ... */
}

int main() {
	char *pw = malloc(32);
	userinfo *ui = (userinfo *)malloc(sizeof(userinfo));
	if (!pw || !ui) {
		fprintf(stderr, "Cannot allocate data structures!\n");
		exit(-1);
	}
	init_ui(ui);
	if (ui->is_root)
		printf("Welcome root!\n");
	else {
		printf("Password: "); 
		fflush(stdout);
		gets(pw);
	}
	if (ui->is_root || strcmp(pw, "CNS")==0) {
		printf("You are authorized to destroy the system now ;-)\n");
	}
}

