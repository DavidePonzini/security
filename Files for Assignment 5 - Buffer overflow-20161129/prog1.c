#include <stdio.h>

int check_password(const char * const pw) {
	return 0;
}

void dangerous_operation() {
	printf("\n\nGood morning master...\n\n");
	exit(0);
}

int main() {
	char password[128];
	printf("You're running with elevated privileges.\nPlease enter the password to continue: ");
	fflush(stdout);
	gets(password);
	if (check_password(password))
		dangerous_operation();
	else
		printf("Go away!!!\n");
}

