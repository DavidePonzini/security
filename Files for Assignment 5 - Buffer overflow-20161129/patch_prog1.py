#!/usr/bin/env python

f = open('prog1', 'rb') 
data = f.read()
f.close()
checkpw_code = '\x55\x89\xe5\xb8\x00\x00\x00\x00'
index = data.find(checkpw_code)
assert index != -1
assert data.find(checkpw_code, index+1) == -1
data = data[:index+4] + 'CNS' + data[index+7:]
f = open('prog1.patched', 'wb')
f.write(data)
f.close()

