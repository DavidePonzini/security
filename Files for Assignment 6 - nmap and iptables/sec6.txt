
Starting Nmap 7.01 ( https://nmap.org ) at 2016-12-11 16:21 CET
NSE: Loaded 253 scripts for scanning.
NSE: Script Pre-scanning.
Initiating NSE at 16:21
NSE: [mtrace] A source IP must be provided through fromip argument.
Completed NSE at 16:22, 10.48s elapsed
Initiating NSE at 16:22
Completed NSE at 16:22, 0.00s elapsed
Pre-scan script results:
| broadcast-igmp-discovery: 
|   192.168.1.254
|     Interface: wlp4s0
|     Version: 2
|     Group: 224.0.0.2
|     Description: All Routers on this Subnet
|   192.168.1.254
|     Interface: wlp4s0
|     Version: 2
|     Group: 224.0.0.22
|     Description: IGMP
|   192.168.1.254
|     Interface: wlp4s0
|     Version: 2
|     Group: 224.0.0.103
|     Description: MDAP
|   192.168.1.253
|     Interface: wlp4s0
|     Version: 2
|     Group: 239.255.255.250
|     Description: Organization-Local Scope (rfc2365)
|_  Use the newtargets script-arg to add the results as targets
| targets-asn: 
|_  targets-asn.asn is a mandatory parameter
Initiating ARP Ping Scan at 16:22
Scanning 192.168.98.128 [1 port]
Completed ARP Ping Scan at 16:22, 0.21s elapsed (1 total hosts)
Initiating Parallel DNS resolution of 1 host. at 16:22
Completed Parallel DNS resolution of 1 host. at 16:22, 0.03s elapsed
Initiating SYN Stealth Scan at 16:22
Scanning 192.168.98.128 [1000 ports]
Discovered open port 139/tcp on 192.168.98.128
Discovered open port 53/tcp on 192.168.98.128
Discovered open port 111/tcp on 192.168.98.128
Discovered open port 21/tcp on 192.168.98.128
Discovered open port 5900/tcp on 192.168.98.128
Discovered open port 3306/tcp on 192.168.98.128
Discovered open port 445/tcp on 192.168.98.128
Discovered open port 23/tcp on 192.168.98.128
Discovered open port 22/tcp on 192.168.98.128
Discovered open port 80/tcp on 192.168.98.128
Discovered open port 25/tcp on 192.168.98.128
Discovered open port 2121/tcp on 192.168.98.128
Discovered open port 512/tcp on 192.168.98.128
Discovered open port 8180/tcp on 192.168.98.128
Discovered open port 513/tcp on 192.168.98.128
Discovered open port 514/tcp on 192.168.98.128
Discovered open port 2049/tcp on 192.168.98.128
Discovered open port 1099/tcp on 192.168.98.128
Discovered open port 6000/tcp on 192.168.98.128
Discovered open port 5432/tcp on 192.168.98.128
Discovered open port 8009/tcp on 192.168.98.128
Discovered open port 6667/tcp on 192.168.98.128
Discovered open port 1524/tcp on 192.168.98.128
Completed SYN Stealth Scan at 16:22, 1.22s elapsed (1000 total ports)
Initiating UDP Scan at 16:22
Scanning 192.168.98.128 [1000 ports]
Increasing send delay for 192.168.98.128 from 0 to 50 due to max_successful_tryno increase to 5
Increasing send delay for 192.168.98.128 from 50 to 100 due to max_successful_tryno increase to 6
Warning: 192.168.98.128 giving up on port because retransmission cap hit (6).
adjust_timeouts2: packet supposedly had rtt of -100056 microseconds.  Ignoring time.
adjust_timeouts2: packet supposedly had rtt of -100056 microseconds.  Ignoring time.
adjust_timeouts2: packet supposedly had rtt of -100027 microseconds.  Ignoring time.
adjust_timeouts2: packet supposedly had rtt of -100027 microseconds.  Ignoring time.
adjust_timeouts2: packet supposedly had rtt of -100020 microseconds.  Ignoring time.
adjust_timeouts2: packet supposedly had rtt of -100020 microseconds.  Ignoring time.
adjust_timeouts2: packet supposedly had rtt of -99964 microseconds.  Ignoring time.
adjust_timeouts2: packet supposedly had rtt of -99964 microseconds.  Ignoring time.
adjust_timeouts2: packet supposedly had rtt of -100020 microseconds.  Ignoring time.
adjust_timeouts2: packet supposedly had rtt of -100020 microseconds.  Ignoring time.
Increasing send delay for 192.168.98.128 from 100 to 200 due to 11 out of 14 dropped probes since last increase.
UDP Scan Timing: About 7.30% done; ETC: 16:29 (0:06:34 remaining)
Increasing send delay for 192.168.98.128 from 200 to 400 due to 11 out of 11 dropped probes since last increase.
Increasing send delay for 192.168.98.128 from 400 to 800 due to 11 out of 11 dropped probes since last increase.
UDP Scan Timing: About 10.61% done; ETC: 16:31 (0:08:34 remaining)
UDP Scan Timing: About 13.69% done; ETC: 16:33 (0:09:34 remaining)
UDP Scan Timing: About 37.20% done; ETC: 16:36 (0:08:59 remaining)
UDP Scan Timing: About 43.56% done; ETC: 16:36 (0:08:15 remaining)
Discovered open port 111/udp on 192.168.98.128
UDP Scan Timing: About 49.49% done; ETC: 16:36 (0:07:28 remaining)
UDP Scan Timing: About 55.23% done; ETC: 16:37 (0:06:42 remaining)
UDP Scan Timing: About 60.67% done; ETC: 16:37 (0:05:57 remaining)
Discovered open port 2049/udp on 192.168.98.128
Discovered open port 137/udp on 192.168.98.128
UDP Scan Timing: About 66.21% done; ETC: 16:37 (0:05:08 remaining)
UDP Scan Timing: About 71.74% done; ETC: 16:37 (0:04:19 remaining)
UDP Scan Timing: About 77.09% done; ETC: 16:37 (0:03:32 remaining)
Discovered open port 53/udp on 192.168.98.128
UDP Scan Timing: About 82.31% done; ETC: 16:37 (0:02:44 remaining)
UDP Scan Timing: About 87.46% done; ETC: 16:37 (0:01:57 remaining)
UDP Scan Timing: About 92.57% done; ETC: 16:37 (0:01:10 remaining)
Completed UDP Scan at 16:38, 980.66s elapsed (1000 total ports)
Initiating Service scan at 16:38
Scanning 53 services on 192.168.98.128
Service scan Timing: About 52.83% done; ETC: 16:39 (0:00:36 remaining)
Completed Service scan at 16:39, 88.55s elapsed (53 services on 1 host)
Initiating OS detection (try #1) against 192.168.98.128
adjust_timeouts2: packet supposedly had rtt of -175704 microseconds.  Ignoring time.
adjust_timeouts2: packet supposedly had rtt of -175704 microseconds.  Ignoring time.
NSE: Script scanning 192.168.98.128.
Initiating NSE at 16:39
Completed NSE at 16:41, 66.04s elapsed
Initiating NSE at 16:41
Completed NSE at 16:41, 0.05s elapsed
Nmap scan report for 192.168.98.128
Host is up (0.00016s latency).
Not shown: 1947 closed ports, 26 open|filtered ports
PORT     STATE SERVICE     VERSION
21/tcp   open  ftp         vsftpd 2.3.4
|_banner: 220 (vsFTPd 2.3.4)
|_ftp-anon: Anonymous FTP login allowed (FTP code 230)
22/tcp   open  ssh         OpenSSH 4.7p1 Debian 8ubuntu1 (protocol 2.0)
|_banner: SSH-2.0-OpenSSH_4.7p1 Debian-8ubuntu1
| ssh-hostkey: 
|   1024 60:0f:cf:e1:c0:5f:6a:74:d6:90:24:fa:c4:d5:6c:cd (DSA)
|_  2048 56:56:24:0f:21:1d:de:a7:2b:ae:61:b1:24:3d:e8:f3 (RSA)
| ssh2-enum-algos: 
|   kex_algorithms: (4)
|       diffie-hellman-group-exchange-sha256
|       diffie-hellman-group-exchange-sha1
|       diffie-hellman-group14-sha1
|       diffie-hellman-group1-sha1
|   server_host_key_algorithms: (2)
|       ssh-rsa
|       ssh-dss
|   encryption_algorithms: (13)
|       aes128-cbc
|       3des-cbc
|       blowfish-cbc
|       cast128-cbc
|       arcfour128
|       arcfour256
|       arcfour
|       aes192-cbc
|       aes256-cbc
|       rijndael-cbc@lysator.liu.se
|       aes128-ctr
|       aes192-ctr
|       aes256-ctr
|   mac_algorithms: (7)
|       hmac-md5
|       hmac-sha1
|       umac-64@openssh.com
|       hmac-ripemd160
|       hmac-ripemd160@openssh.com
|       hmac-sha1-96
|       hmac-md5-96
|   compression_algorithms: (2)
|       none
|_      zlib@openssh.com
23/tcp   open  telnet      Linux telnetd
|_banner: \xFF\xFD\x18\xFF\xFD \xFF\xFD#\xFF\xFD'
| telnet-encryption: 
|_  Telnet server does not support encryption
25/tcp   open  smtp        Postfix smtpd
|_banner: 220 metasploitable.localdomain ESMTP Postfix (Ubuntu)
|_smtp-commands: metasploitable.localdomain, PIPELINING, SIZE 10240000, VRFY, ETRN, STARTTLS, ENHANCEDSTATUSCODES, 8BITMIME, DSN, 
| ssl-cert: Subject: commonName=ubuntu804-base.localdomain/organizationName=OCOSA/stateOrProvinceName=There is no such thing outside US/countryName=XX
| Issuer: commonName=ubuntu804-base.localdomain/organizationName=OCOSA/stateOrProvinceName=There is no such thing outside US/countryName=XX
| Public Key type: rsa
| Public Key bits: 1024
| Signature Algorithm: sha1WithRSAEncryption
| Not valid before: 2010-03-17T14:07:45
| Not valid after:  2010-04-16T14:07:45
| MD5:   dcd9 ad90 6c8f 2f73 74af 383b 2540 8828
|_SHA-1: ed09 3088 7066 03bf d5dc 2373 99b4 98da 2d4d 31c6
|_ssl-date: 2016-11-30T14:32:16+00:00; -11d01h07m47s from scanner time.
53/tcp   open  domain      ISC BIND 9.4.2
80/tcp   open  http        Apache httpd 2.2.8 ((Ubuntu) DAV/2)
|_http-apache-negotiation: mod_negotiation enabled.
| http-auth-finder: 
| Spidering limited to: maxdepth=3; maxpagecount=20; withinhost=192.168.98.128
|   url                                                             method
|   http://192.168.98.128/phpMyAdmin/                               FORM
|   http://192.168.98.128/mutillidae/./index.php?page=register.php  FORM
|_  http://192.168.98.128/mutillidae/?page=user-info.php            FORM
| http-comments-displayer: 
| Spidering limited to: maxdepth=3; maxpagecount=20; withinhost=192.168.98.128
|     
|     Path: http://192.168.98.128/mutillidae/./index.php?page=html5-storage.php
|     Line number: 693
|     Comment: 
|         
|         //-->
|     
|     Path: http://192.168.98.128/mutillidae/./index.php?page=html5-storage.php
|     Line number: 554
|     Comment: 
|         /* String */
|     
|     Path: http://192.168.98.128/mutillidae/./index.php?page=set-background-color.php
|     Line number: 2
|     Comment: 
|         <!-- I think the database password is set to blank or perhaps samurai.
|         			It depends on whether you installed this web app from irongeeks site or
|         			are using it inside Kevin Johnsons Samurai web testing framework. 
|         			It is ok to put the password in HTML comments because no user will ever see 
|         			this comment. I remember that security instructor saying we should use the 
|         			framework comment symbols (ASP.NET, JAVA, PHP, Etc.) 
|         			rather than HTML comments, but we all know those 
|         			security instructors are just making all this up. -->
|     
|     Path: http://192.168.98.128/mutillidae/./index.php?page=set-background-color.php
|     Line number: 488
|     Comment: 
|         <!-- Begin Content -->
|     
|     Path: http://192.168.98.128/mutillidae/./index.php?page=html5-storage.php
|     Line number: 686
|     Comment: 
|         <!--
|         	try{
|         		document.getElementById("idDOMStorageKeyInput").focus();
|         		init();
|         	}catch(/*Exception*/ e){
|         		alert("Error trying to set focus: " + e.message);
|         	}// end try
|         //-->
|     
|     Path: http://192.168.98.128/mutillidae/./javascript/bookmark-site.js
|     Line number: 2
|     Comment: 
|         /***********************************************
|         	* Bookmark site script- \xA9 Dynamic Drive DHTML code library (www.dynamicdrive.com)
|         	* This notice MUST stay intact for legal use
|         	* Visit Dynamic Drive at http://www.dynamicdrive.com/ for full source code
|         	***********************************************/
|     
|     Path: http://192.168.98.128/mutillidae/./index.php?page=set-background-color.php
|     Line number: 35
|     Comment: 
|          //"markup" or ["container_id", "path_to_menu_file"]
|     
|     Path: http://192.168.98.128/mutillidae/./index.php?page=set-background-color.php
|     Line number: 31
|     Comment: 
|          //menu DIV id
|     
|     Path: http://192.168.98.128/mutillidae/./javascript/bookmark-site.js
|     Line number: 8
|     Comment: 
|         /* Modified heavily by Jeremy Druin */
|     
|     Path: http://192.168.98.128/mutillidae/./index.php?page=set-background-color.php
|     Line number: 23
|     Comment: 
|         /***********************************************
|         		* Smooth Navigational Menu- (c) Dynamic Drive DHTML code library (www.dynamicdrive.com)
|         		* This notice MUST stay intact for legal use
|         		* Visit Dynamic Drive at http://www.dynamicdrive.com/ for full source code
|         		***********************************************/
|     
|     Path: http://192.168.98.128/mutillidae/./index.php?page=password-generator.php&username=anonymous
|     Line number: 491
|     Comment: 
|         /*HTMLFormElement*/
|     
|     Path: http://192.168.98.128/phpMyAdmin/
|     Line number: 44
|     Comment: 
|         <!-- Login form -->
|     
|     Path: http://192.168.98.128/mutillidae/./index.php?page=set-background-color.php
|     Line number: 32
|     Comment: 
|          //Horizontal or vertical menu: Set to "h" or "v"
|     
|     Path: http://192.168.98.128/mutillidae/./index.php?page=pen-test-tool-lookup.php
|     Line number: 584
|     Comment: 
|         /*Exception*/
|     
|     Path: http://192.168.98.128/mutillidae/./index.php?page=html5-storage.php
|     Line number: 510
|     Comment: 
|         /* 
|         		The Storage interface of the browser API
|         	
|         		interface Storage {
|         			  readonly attribute unsigned long length;
|         			  DOMString? key(unsigned long index);
|         			  getter DOMString getItem(DOMString key);
|         			  setter creator void setItem(DOMString key, DOMString value);
|         			  deleter void removeItem(DOMString key);
|         			  void clear();
|         		};
|         	*/
|     
|     Path: http://192.168.98.128/phpMyAdmin/
|     Line number: 13
|     Comment: 
|         
|         //<![CDATA[
|     
|     Path: http://192.168.98.128/mutillidae/index.php?page=captured-data.php
|     Line number: 509
|     Comment: 
|         <!-- BEGIN HTML OUTPUT  -->
|     
|     Path: http://192.168.98.128/phpMyAdmin/
|     Line number: 66
|     Comment: 
|         
|         // <![CDATA[
|     
|     Path: http://192.168.98.128/mutillidae/./index.php?page=set-background-color.php
|     Line number: 547
|     Comment: 
|         <!-- End Content -->
|     
|     Path: http://192.168.98.128/mutillidae/index.php?page=add-to-your-blog.php
|     Line number: 492
|     Comment: 
|         /* HTMLForm */
|     
|     Path: http://192.168.98.128/phpMyAdmin/
|     Line number: 79
|     Comment: 
|         
|         // ]]>
|     
|     Path: http://192.168.98.128/mutillidae/./index.php?page=set-background-color.php
|     Line number: 33
|     Comment: 
|          //class added to menu's outer DIV
|     
|     Path: http://192.168.98.128/phpMyAdmin/
|     Line number: 18
|     Comment: 
|         
|_        //]]>
|_http-date: Wed, 30 Nov 2016 14:32:13 GMT; -11d01h07m46s from local time.
| http-grep: 
|   (1) http://192.168.98.128/dvwa/: 
|     (1) ip: 
|       + 192.168.98.128
|   (1) http://192.168.98.128/mutillidae/?page=credits.php: 
|     (1) email: 
|_      + mutillidae-development@gmail.com
| http-headers: 
|   Date: Wed, 30 Nov 2016 14:32:18 GMT
|   Server: Apache/2.2.8 (Ubuntu) DAV/2
|   X-Powered-By: PHP/5.2.4-2ubuntu5.10
|   Connection: close
|   Content-Type: text/html
|   
|_  (Request type: HEAD)
| http-methods: 
|_  Supported Methods: GET HEAD POST OPTIONS
|_http-mobileversion-checker: No mobile version detected.
| http-php-version: Versions from logo query (less accurate): 5.1.3 - 5.1.6, 5.2.0 - 5.2.17
| Versions from credits query (more accurate): 5.2.3 - 5.2.5
|_Version from header x-powered-by: PHP/5.2.4-2ubuntu5.10
|_http-referer-checker: Couldn't find any cross-domain scripts.
|_http-server-header: Apache/2.2.8 (Ubuntu) DAV/2
|_http-title: Metasploitable2 - Linux
|_http-trace: TRACE is enabled
| http-traceroute: 
|_  Possible reverse proxy detected.
| http-useragent-tester: 
|   
|     Allowed User Agents:
|     Mozilla/5.0 (compatible; Nmap Scripting Engine; https://nmap.org/book/nse.html)
|     libwww
|     lwp-trivial
|     libcurl-agent/1.0
|     PHP/
|     Python-urllib/2.5
|     GT::WWW
|     Snoopy
|     MFC_Tear_Sample
|     HTTP::Lite
|     PHPCrawl
|     URI::Fetch
|     Zend_Http_Client
|     http client
|     PECL::HTTP
|     Wget/1.13.4 (linux-gnu)
|     WWW-Mechanize/1.34
|_  
|_http-xssed: No previously reported XSS vuln.
111/tcp  open  rpcbind     2 (RPC #100000)
| nfs-showmount: 
|_  / *
| rpcinfo: 
|   program version   port/proto  service
|   100000  2            111/tcp  rpcbind
|   100000  2            111/udp  rpcbind
|   100003  2,3,4       2049/tcp  nfs
|   100003  2,3,4       2049/udp  nfs
|   100005  1,2,3      44291/udp  mountd
|   100005  1,2,3      59829/tcp  mountd
|   100021  1,3,4      34237/tcp  nlockmgr
|   100021  1,3,4      39310/udp  nlockmgr
|   100024  1          46153/udp  status
|_  100024  1          47849/tcp  status
139/tcp  open  netbios-ssn Samba smbd 3.X (workgroup: WORKGROUP)
445/tcp  open  netbios-ssn Samba smbd 3.X (workgroup: WORKGROUP)
512/tcp  open  exec        netkit-rsh rexecd
|_banner: \x01Where are you?
513/tcp  open  login
514/tcp  open  tcpwrapped
1099/tcp open  java-rmi    Java RMI Registry
1524/tcp open  shell       Metasploitable root shell
|_banner: root@metasploitable:/#
2049/tcp open  nfs         2-4 (RPC #100003)
| rpcinfo: 
|   program version   port/proto  service
|   100000  2            111/tcp  rpcbind
|   100000  2            111/udp  rpcbind
|   100003  2,3,4       2049/tcp  nfs
|   100003  2,3,4       2049/udp  nfs
|   100005  1,2,3      44291/udp  mountd
|   100005  1,2,3      59829/tcp  mountd
|   100021  1,3,4      34237/tcp  nlockmgr
|   100021  1,3,4      39310/udp  nlockmgr
|   100024  1          46153/udp  status
|_  100024  1          47849/tcp  status
2121/tcp open  ftp         ProFTPD 1.3.1
|_banner: 220 ProFTPD 1.3.1 Server (Debian) [::ffff:192.168.98.128]
3306/tcp open  mysql       MySQL 5.0.51a-3ubuntu5
| banner: >\x00\x00\x00\x0A5.0.51a-3ubuntu5\x00\x14\x00\x00\x00K=0x/|F*\x
|_00,\xAA\x08\x02\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\...
| mysql-info: 
|   Protocol: 53
|   Version: .0.51a-3ubuntu5
|   Thread ID: 11
|   Capabilities flags: 43564
|   Some Capabilities: SupportsTransactions, Support41Auth, SwitchToSSLAfterHandshake, LongColumnFlag, SupportsCompression, Speaks41ProtocolNew, ConnectWithDatabase
|   Status: Autocommit
|_  Salt: 3Lf<9IAL;"7/$fUY>j|O
5432/tcp open  postgresql  PostgreSQL DB 8.3.0 - 8.3.7
5900/tcp open  vnc         VNC (protocol 3.3)
|_banner: RFB 003.003
| vnc-info: 
|   Protocol version: 3.3
|   Security types: 
|_    Unknown security type (33554432)
6000/tcp open  X11         (access denied)
6667/tcp open  irc         Unreal ircd
| banner: :irc.Metasploitable.LAN NOTICE AUTH :*** Looking up your hostna
|_me...\x0D\x0A:irc.Metasploitable.LAN NOTICE AUTH :*** Couldn't resol...
8009/tcp open  ajp13       Apache Jserv (Protocol v1.3)
| ajp-headers: 
|_  Content-Type: text/html;charset=ISO-8859-1
|_ajp-methods: Failed to get a valid response for the OPTION request
| ajp-request: 
| AJP/1.3 200 OK
| Content-Type: text/html;charset=ISO-8859-1
| 
| iguring and using Tomcat</li>
|                <li><b><a href="mailto:dev@tomcat.apache.org">dev@tomcat.apache.org</a></b> for developers working on Tomcat</li>
|            </ul>
| 
|             <p>Thanks for using Tomcat!</p>
| 
|             <p id="footer"><img src="tomcat-power.gif" width="77" height="80" alt="Powered by Tomcat"/><br/>
| 	    &nbsp;
| 
| 	    Copyright &copy; 1999-2005 Apache Software Foundation<br/>
|             All Rights Reserved
|             </p>
|         </td>
| 
|     </tr>
| </table>
| 
| </body>
|_</html>
8180/tcp open  http        Apache Tomcat/Coyote JSP engine 1.1
| http-auth-finder: 
| Spidering limited to: maxdepth=3; maxpagecount=20; withinhost=192.168.98.128
|   url                                        method
|   http://192.168.98.128:8180/manager/status  HTTP: Basic
|_  http://192.168.98.128:8180/manager/html    HTTP: Basic
| http-comments-displayer: 
| Spidering limited to: maxdepth=3; maxpagecount=20; withinhost=192.168.98.128
|     
|     Path: http://192.168.98.128:8180/tomcat-docs/changelog.html
|     Line number: 3
|     Comment: 
|         <!--APACHE LOGO-->
|     
|     Path: http://192.168.98.128:8180/tomcat-docs/changelog.html
|     Line number: 3
|     Comment: 
|         <!--RIGHT SIDE MAIN BODY-->
|     
|     Path: http://192.168.98.128:8180/jsp-examples/num/numguess.html
|     Line number: 2
|     Comment: 
|         <!--
|          Licensed to the Apache Software Foundation (ASF) under one or more
|           contributor license agreements.  See the NOTICE file distributed with
|           this work for additional information regarding copyright ownership.
|           The ASF licenses this file to You under the Apache License, Version 2.0
|           (the "License"); you may not use this file except in compliance with
|           the License.  You may obtain a copy of the License at
|         
|               http://www.apache.org/licenses/LICENSE-2.0
|         
|           Unless required by applicable law or agreed to in writing, software
|           distributed under the License is distributed on an "AS IS" BASIS,
|           WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
|           See the License for the specific language governing permissions and
|           limitations under the License.
|         
|           Number Guess Game
|           Written by Jason Hunter, CTO, K&A Software
|           http://www.servlets.com
|         -->
|     
|     Path: http://192.168.98.128:8180/jsp-examples/
|     Line number: 56
|     Comment: 
|         <!--<tr VALIGN=TOP>
|         <td WIDTH="30"><img SRC="images/read.gif" height=24 width=24></td>
|         
|         <td>Read more about this feature</td>
|         -->
|     
|     Path: http://192.168.98.128:8180/tomcat-docs/changelog.html
|     Line number: 3
|     Comment: 
|         <!--LEFT SIDE NAVIGATION-->
|     
|     Path: http://192.168.98.128:8180/
|     Line number: 25
|     Comment: 
|         /*<![CDATA[*/
|     
|     Path: http://192.168.98.128:8180/tomcat-docs/changelog.html
|     Line number: 1
|     Comment: 
|         <!--PAGE HEADER-->
|     
|     Path: http://192.168.98.128:8180/manager/html
|     Line number: 1
|     Comment: 
|         <!--H1 {font-family:Tahoma,Arial,sans-serif;color:white;background-color:#525D76;font-size:22px;} H2 {font-family:Tahoma,Arial,sans-serif;color:white;background-color:#525D76;font-size:16px;} H3 {font-family:Tahoma,Arial,sans-serif;color:white;background-color:#525D76;font-size:14px;} BODY {font-family:Tahoma,Arial,sans-serif;color:black;background-color:white;} B {font-family:Tahoma,Arial,sans-serif;color:white;background-color:#525D76;} P {font-family:Tahoma,Arial,sans-serif;background:white;color:black;font-size:12px;}A {color : black;}A.name {color : black;}HR {color : #525D76;}-->
|     
|     Path: http://192.168.98.128:8180/jsp-examples/jsp2/el/implicit-objects.html
|     Line number: 2
|     Comment: 
|         <!--
|           Licensed to the Apache Software Foundation (ASF) under one or more
|           contributor license agreements.  See the NOTICE file distributed with
|           this work for additional information regarding copyright ownership.
|           The ASF licenses this file to You under the Apache License, Version 2.0
|           (the "License"); you may not use this file except in compliance with
|           the License.  You may obtain a copy of the License at
|         
|               http://www.apache.org/licenses/LICENSE-2.0
|         
|           Unless required by applicable law or agreed to in writing, software
|           distributed under the License is distributed on an "AS IS" BASIS,
|           WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
|           See the License for the specific language governing permissions and
|           limitations under the License.
|         -->
|     
|     Path: http://192.168.98.128:8180/
|     Line number: 84
|     Comment: 
|         /*]]>*/
|     
|     Path: http://192.168.98.128:8180/tomcat-docs/changelog.html
|     Line number: 3690
|     Comment: 
|         <!--PAGE FOOTER-->
|     
|     Path: http://192.168.98.128:8180/
|     Line number: 110
|     Comment: 
|         <!-- Table of Contents -->
|     
|     Path: http://192.168.98.128:8180/tomcat-docs/changelog.html
|     Line number: 3
|     Comment: 
|         <!--HEADER SEPARATOR-->
|     
|     Path: http://192.168.98.128:8180/tomcat-docs/changelog.html
|     Line number: 3690
|     Comment: 
|         <!--FOOTER SEPARATOR-->
|     
|     Path: http://192.168.98.128:8180/
|     Line number: 192
|     Comment: 
|         <!-- Body -->
|     
|     Path: http://192.168.98.128:8180/tomcat-docs/changelog.html
|     Line number: 1
|     Comment: 
|         <!--PROJECT LOGO-->
|     
|     Path: http://192.168.98.128:8180/
|     Line number: 90
|     Comment: 
|         <!-- Header -->
|     
|     Path: http://192.168.98.128:8180/jsp-examples/
|     Line number: 24
|     Comment: 
|         <!--
|           Copyright (c) 1999 The Apache Software Foundation.  All rights 
|           reserved.
|         -->
|     
|     Path: http://192.168.98.128:8180/jsp-examples/include/include.jsp
|     Line number: 2
|     Comment: 
|         <!--
|          Licensed to the Apache Software Foundation (ASF) under one or more
|           contributor license agreements.  See the NOTICE file distributed with
|           this work for additional information regarding copyright ownership.
|           The ASF licenses this file to You under the Apache License, Version 2.0
|           (the "License"); you may not use this file except in compliance with
|           the License.  You may obtain a copy of the License at
|         
|               http://www.apache.org/licenses/LICENSE-2.0
|         
|           Unless required by applicable law or agreed to in writing, software
|           distributed under the License is distributed on an "AS IS" BASIS,
|           WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
|           See the License for the specific language governing permissions and
|           limitations under the License.
|_        -->
|_http-date: Wed, 30 Nov 2016 14:32:15 GMT; -11d01h07m43s from local time.
|_http-favicon: Apache Tomcat
| http-grep: 
|   (2) http://192.168.98.128:8180/: 
|     (2) email: 
|       + users@tomcat.apache.org
|       + dev@tomcat.apache.org
|   (3) http://192.168.98.128:8180/tomcat-docs/changelog.html: 
|     (3) email: 
|       + remm@apache.org
|       + yoavs@apache.org
|_      + fhanik@apache.org
| http-headers: 
|   Server: Apache-Coyote/1.1
|   Content-Type: text/html;charset=ISO-8859-1
|   Date: Wed, 30 Nov 2016 14:32:14 GMT
|   Connection: close
|   
|_  (Request type: HEAD)
| http-methods: 
|_  Supported Methods: GET HEAD POST OPTIONS
|_http-mobileversion-checker: No mobile version detected.
|_http-referer-checker: Couldn't find any cross-domain scripts.
|_http-server-header: Apache-Coyote/1.1
|_http-title: Apache Tomcat/5.5
| http-traceroute: 
|_  Possible reverse proxy detected.
| http-useragent-tester: 
|   
|     Allowed User Agents:
|     Mozilla/5.0 (compatible; Nmap Scripting Engine; https://nmap.org/book/nse.html)
|     libwww
|     lwp-trivial
|     libcurl-agent/1.0
|     PHP/
|     Python-urllib/2.5
|     GT::WWW
|     Snoopy
|     MFC_Tear_Sample
|     HTTP::Lite
|     PHPCrawl
|     URI::Fetch
|     Zend_Http_Client
|     http client
|     PECL::HTTP
|     Wget/1.13.4 (linux-gnu)
|     WWW-Mechanize/1.34
|_  
|_http-xssed: No previously reported XSS vuln.
53/udp   open  domain      ISC BIND 9.4.2
| dns-nsid: 
|_  bind.version: 9.4.2
|_dns-recursion: Recursion appears to be enabled
111/udp  open  rpcbind     2 (RPC #100000)
| nfs-showmount: 
|_  / *
| rpcinfo: 
|   program version   port/proto  service
|   100000  2            111/tcp  rpcbind
|   100000  2            111/udp  rpcbind
|   100003  2,3,4       2049/tcp  nfs
|   100003  2,3,4       2049/udp  nfs
|   100005  1,2,3      44291/udp  mountd
|   100005  1,2,3      59829/tcp  mountd
|   100021  1,3,4      34237/tcp  nlockmgr
|   100021  1,3,4      39310/udp  nlockmgr
|   100024  1          46153/udp  status
|_  100024  1          47849/tcp  status
137/udp  open  netbios-ns  Microsoft Windows XP netbios-ssn
2049/udp open  nfs         2-4 (RPC #100003)
| rpcinfo: 
|   program version   port/proto  service
|   100000  2            111/tcp  rpcbind
|   100000  2            111/udp  rpcbind
|   100003  2,3,4       2049/tcp  nfs
|   100003  2,3,4       2049/udp  nfs
|   100005  1,2,3      44291/udp  mountd
|   100005  1,2,3      59829/tcp  mountd
|   100021  1,3,4      34237/tcp  nlockmgr
|   100021  1,3,4      39310/udp  nlockmgr
|   100024  1          46153/udp  status
|_  100024  1          47849/tcp  status
MAC Address: 00:0C:29:9D:4D:DF (VMware)
Device type: general purpose
Running: Linux 2.6.X
OS CPE: cpe:/o:linux:linux_kernel:2.6
OS details: Linux 2.6.9 - 2.6.33
Uptime guess: 0.063 days (since Sun Dec 11 15:09:53 2016)
Network Distance: 1 hop
TCP Sequence Prediction: Difficulty=213 (Good luck!)
IP ID Sequence Generation: All zeros
Service Info: Hosts:  metasploitable.localdomain, localhost, irc.Metasploitable.LAN, METASPLOITABLE; OSs: Unix, Linux, Windows XP; CPE: cpe:/o:linux:linux_kernel, cpe:/o:microsoft:windows_xp

Host script results:
|_fcrdns: FAIL (No PTR record)
| firewalk: 
| HOP  HOST          PROTOCOL  BLOCKED PORTS
|_0    192.168.98.1  udp       13,68-69,138,500,1066,3052,5060,16449,16503
|_ipidseq: All zeros
|_msrpc-enum: NT_STATUS_OBJECT_NAME_NOT_FOUND
| nbstat: NetBIOS name: METASPLOITABLE, NetBIOS user: <unknown>, NetBIOS MAC: <unknown> (unknown)
| Names:
|   METASPLOITABLE<00>   Flags: <unique><active>
|   METASPLOITABLE<03>   Flags: <unique><active>
|   METASPLOITABLE<20>   Flags: <unique><active>
|   \x01\x02__MSBROWSE__\x02<01>  Flags: <group><active>
|   WORKGROUP<00>        Flags: <group><active>
|   WORKGROUP<1d>        Flags: <unique><active>
|_  WORKGROUP<1e>        Flags: <group><active>
|_path-mtu: PMTU == 1500
| qscan: 
| PORT  FAMILY  MEAN (us)  STDDEV  LOSS (%)
| 1     0       149.90     22.57   0.0%
| 21    0       148.50     32.51   0.0%
| 22    0       143.20     25.52   0.0%
| 23    0       139.50     33.77   0.0%
| 25    0       168.33     44.51   10.0%
| 53    0       140.10     22.19   0.0%
| 80    0       158.20     21.02   0.0%
| 111   0       139.20     23.92   0.0%
|_139   0       153.90     28.81   0.0%
| smb-mbenum: 
|_  ERROR: Failed to connect to browser service: SMB: ERROR: Server returned less data than it was supposed to (one or more fields are missing); aborting [12]
| smb-os-discovery: 
|   OS: Unix (Samba 3.0.20-Debian)
|   NetBIOS computer name: 
|   Workgroup: WORKGROUP
|_  System time: 2016-11-30T09:32:16-05:00
| traceroute-geolocation: 
|   HOP  RTT   ADDRESS         GEOLOCATION
|_  1    0.16  192.168.98.128  - ,- 

TRACEROUTE
HOP RTT     ADDRESS
1   0.16 ms 192.168.98.128

NSE: Script Post-scanning.
Initiating NSE at 16:41
Completed NSE at 16:41, 0.00s elapsed
Initiating NSE at 16:41
Completed NSE at 16:41, 0.00s elapsed
Read data files from: /usr/bin/../share/nmap
OS and Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 1150.50 seconds
           Raw packets sent: 2664 (100.356KB) | Rcvd: 2080 (101.966KB)

