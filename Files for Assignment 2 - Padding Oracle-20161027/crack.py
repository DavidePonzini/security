#!/usr/bin/env python3

# @author	Davide Ponzini


from oracle import encrypt, is_padding_ok, BLOCK_SIZE


# Cracks a single byte in a given message
#	@param data				the message to crack
#	@param position			index of the byte to crack
#	@param decoded_bytes	list containg already cracked bytes
#	@return					the cracked byte at the given position
#	@exception Exception	if we could not crack the given byte
def _crack_nth_byte(data, position, decoded_bytes):
	extra = []
	for idx, byte in enumerate(decoded_bytes):
		offset = BLOCK_SIZE + len(decoded_bytes) - idx
		value = byte ^ data[-offset] ^ position
		extra.append(value)
	
	for value in range(1, 256):
		if _try_value(value, position, data, extra):
			return value ^ position

	# since all possible values failed, the original message alreay has the one we need
	# check to be sure there are no errors
	if _try_value(0, position, data, extra):
		return position

	raise Exception('Couldn\'t crack byte at position {}'.format(position))


# Builds a message, replacing some values with ours
#	@param data			the original message
#	@param position		where to insert our bytes
#	@param values		our modified bytes
#	@return				the message to send
def _build_message(data, position, values):
	offset_start = BLOCK_SIZE + position
	offset_end = BLOCK_SIZE

	return data[: -offset_start] + bytes(values) + data[-offset_end :]


# Tries cracking a byte at the given position using the value supplied
#	@param value			the value to use for cracking
#	@param position			index of the byte to crack
#	@param data				the message to crack
#	@param extra			list of bytes to append after `value`
#	@return					whether we cracked the byte using `value`
def _try_value(value, position, data, extra):
	offset = BLOCK_SIZE + position

	byte = value ^ data[-offset]

	message = _build_message(data, position, [byte] + extra)

	return is_padding_ok(message)


# Cracks a single block
#	@param data		the message to crack, with the last block to be cracked
#	@return			the cracked block
def _crack_block(data):
	decoded_bytes = []

	for idx in range(0, BLOCK_SIZE):
		byte = _crack_nth_byte(data, idx+1, decoded_bytes)
		decoded_bytes = [byte] + decoded_bytes

	return decoded_bytes


# Cracks a given message
#	@param message		the message to crack
#	@return				the cracked message
def crack_message(message):
	decoded_message = []

	blocks_num = len(message) // BLOCK_SIZE
	# Subtract 1 for the IV
	blocks_num -= 1
	for idx in range(0, blocks_num):
		offset = BLOCK_SIZE * idx

		if offset == 0:
			data = message
		else:
			data = message[: -offset]

		block = _crack_block(data)
		decoded_message = block + decoded_message
	
	return remove_padding(bytes(decoded_message))


# Removes padding from a cracked message
#	@param message		the message to remove padding from
#	@return				the message without padding
def remove_padding(message):
	pad_len = message[-1]
	return message[: -pad_len]
	

def test_the_attack():
	messages = (b'Attack at dawn', b'CNS', b'', b'Giovanni',
				b"In symmetric cryptography, the padding oracle attack can be applied to the CBC mode of operation," +
				b"where the \"oracle\" (usually a server) leaks data about whether the padding of an encrypted " +
				b"message is correct or not. Such data can allow attackers to decrypt (and sometimes encrypt) " +
				b"messages through the oracle using the oracle's key, without knowing the encryption key")
	for msg in messages:
		print('Testing:', msg)
		cracked_ct = crack_message(encrypt(msg))
		assert cracked_ct == msg


if __name__ == '__main__':
	test_the_attack()
